# Install necessary components to be able to use powershell in unix style :)
Import-Module -Name PSReadLine

# First thing to do is to create ps $Profile file
# testing if exists
$exists = Test-Path $profile

if (-not $exists) {
    New-Item $profile -type file -force
}

'# Set Up bash exit shortcut
Set-PSReadlineKeyHandler -Chord 'Ctrl+d' -Function DeleteCharOrExit
Set-PSReadlineOption -BellStyle Visual
Set-PSReadlineOption -ContinuationPrompt '> '

# sudo functionality
function sudo {
    $file, [string]$arguments = $args;
    $psi = new-object System.Diagnostics.ProcessStartInfo $file;
    $psi.Arguments = $arguments;
    $psi.Verb = "runas";
    #$psi.WorkingDirectory = get-location;
    [System.Diagnostics.Process]::Start($psi);
}

function su {
    $psi = new-object System.Diagnostics.ProcessStartInfo powershell.exe;
    $psi.Arguments = "-noexit & {cd $(pwd)}";
    $psi.Verb = "runas";
    [System.Diagnostics.Process]::Start($psi);
}

function gitHistory { git log --all --graph --decorate --oneline }


function gitCommit {
	Param (
		[Parameter(Mandatory=$False)][string]$files = ".",
		[Parameter(Mandatory=$False)][string]$commitMsg = "Random Commit Msg",
		[Parameter(Mandatory=$False)][string]$commitMods = ""
	)
	git add $files
	git commit -m $commitMsg $commitMods
}' | Out-File $profile -Append
